### Conway's Game of life  
Author: **Mihai Andries** (mihai@andries.eu)  
Date: December 2008  
Université de Strasbourg  

### Compilation and launch

```C
cd src
make 
./game
```