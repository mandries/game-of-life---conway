#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "calc.h"
#include "print.h"
#include "menu.h"
#define TITLE 	printf("\n\t|============================|"); \
		printf("\n\t|:::::::   Conway's   :::::::|"); \
		printf("\n\t|:::::   Game of Life   :::::|"); \
		printf("\n\t|============================|\n");
int stage;
static int choice;
char buffer[1024];

//Function used to safely read a positive integer
int safe_read_pos_integer(int a)
  	{
	int i;
  	scanf("%1022s",buffer); //guarantees buffer will not overflow
   	int len=strlen(buffer);
  	for(i=0;i<len;i++)
  		{
  		if ((buffer[i]<'0') || (buffer[i]>'9'))
 			{
 			buffer[0]=0;
 			break;
 			}
 		}
  	if (buffer[0])	{
			sscanf(buffer,"%d",&a);
			return a;
			}
  	else 	{
		printf("Invalid input \n");
		return -1;
		}		
 	}


//Menu function
void menu ()
	{
	int s_age=0, s_walls=0, s_torus=0, s_load=0;
	do	{
		choice=0;
		system("clear");
		TITLE
		printf("\n\tPlease select action:\n");
		printf("\t1. Generate world\n");
		printf("\t2. Load world from file\n");
		printf("\t3. Basic rules\n");
		printf("\t4. Quit\n");
		scanf("%s",buffer);
		if (strlen(buffer)>1) 
			printf("\nInvalid choice, len=%d\n",strlen(buffer));
		else 	{
			sscanf(buffer, "%d", &choice);
			
			switch (choice)
				{
				case 1 :	game(s_load, s_walls, s_torus, s_age);
						break;
				case 2 :	{
						s_load=1; //Load world from file
						game(s_load, s_walls, s_torus, s_age);
						}
						break;
				case 3 :	//Basic Rules
						{
						printf("\nThe Game of Life is a cellular automaton, devised by the British 	mathematician John Horton Conway in 1970.");
						printf("\nThe universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, live or dead.");
						printf("\nEvery cell interacts with its eight neighbours, which are the cells that are directly horizontally, vertically, or diagonally adjacent.");
						printf("\nAt each step in time, the following transitions occur:");
 						printf("\n:: 1. Any live cell with fewer than two live neighbours dies, as if by needs caused by underpopulation.");
						printf("\n:: 2. Any live cell with more than three live neighbours dies, as if by overcrowding.");
						printf("\n:: 3. Any live cell with two or three live neighbours lives, unchanged, to the next generation.");
						printf("\n:: 4. Any tile with exactly three live neighbours cells will be populated with a living cell.");
						printf("\nThe initial pattern constitutes the 'seed' of the system.");
						printf("\nutes the 'seed' of the system. The first generation is created by applying the above rules simultaneously to every cell in the seed — births and deaths happen simultaneously, and the discrete moment at which this happens is sometimes called a tick.");
						printf("\n(In other words, each generation is a pure function of the one before.)");
						printf("\nThe rules continue to be applied repeatedly to create further generations.\n\n");
						printf("Press ENTER to return to Main menu");
						getc(stdin);
						getc(stdin);
						system("clear");
						}
						break;
				case 4 :	//Quit game
						exit(0);
						break;
				default :	{
 						printf("\n\t Invalid choice.");
						system("clear");
						}
				}
			}
		} while (choice!=4);
	}


void game(int s_load, int s_walls, int s_torus, int s_age)
	{
	int age,dead,qtt=0,i,j,k;
		//age = used as max age value, after which the cell dies
		//dead = number of dead cells to introduce
		//qtt = number of living cells to introduce
	stage=0; //Stage number
	char filename[2000];
	int total=0; //total = number of living cells already introduced (used in the loop)
	int r;	//r = used as the random generated number (used in the loop)
	board world;

	//If game generated (not loaded)
	if (s_load==0)
		{
		do	{
			choice=0;
			system("clear");
			TITLE
			printf("\n\tPlease select action:\n");
			printf("\t1. Start game\n");
			//Options
			if (s_age==0) printf("\t2. Activate age of cells\n");
	 			else printf("\t2. Deactivate age of cells\n");
			if (s_walls==0) printf("\t3. Activate walls\n");
	 			else printf("\t3. Deactivate walls\n");
			if (s_torus==0) printf("\t4. Activate torus world\n");
	 			else printf("\t4. Deactivate torus world\n");

			scanf("%s",buffer);
			if (strlen(buffer)>1) 
				printf("\nInvalid choice, len=%d",strlen(buffer));
			else 	{
				sscanf(buffer, "%d", &choice);
			
				switch (choice)
					{
					case 1 :	break;
					case 2 :	{
 							if (s_age==0) 
								s_age=1; //Activate age of cells
 							else
		 						s_age=0; //Deactivate age of cells
							}
							break;
					case 3 :	{
 							if (s_walls==0)
								s_walls=1; //Activate walls
 							else
		 						s_walls=0; //Deactivate walls
							}
							break;
					case 4 :	{
 							if (s_torus==0)
								s_torus=1; //Activate torus world
 							else
		 						s_torus=0; //Deactivate torus world
							}
							break;
					default :	{
 							printf("\nInvalid choice");
							}
					}
				}
			}while (choice!=1);

		//Safe-Reading height
		do 	{
			world.height=0;
			printf("\tWhat is the width of the game-world?\n");
			scanf("%s",buffer);
			if (strlen(buffer)>2) 
				printf("Please introduce a value less than 38 (to avoid breaking the lines fault to monitor size)\n");
			else 	{
				sscanf(buffer, "%d", &world.width);
				if ((world.width<1) || (world.width>38))
					printf("Please introduce between 1 and 38 (to avoid breaking the lines fault to monitor size)\n");
				else printf("Height: %d",world.width);
				}
			} while ((world.width<1) || (world.width>38));
		
		//Safe-reading width
		do	{
			printf("\tWhat is the height of the game-world?\n");
			world.height=safe_read_pos_integer(world.height);
			} while(world.height<0);
			
			
		//Creating a loop for demanding the number of living cells in input
		do	{
			do	{
				printf("\nHow many living cells do you want to create? (max %d)\n",world.height*world.width);
				qtt=safe_read_pos_integer(qtt);
				} while(qtt<0);

			//This getc(stdin); is used to anihilate the enter after reading the number of walls to introduce
			//Needed for obstaining the program from jumping directly to the 2nd stage
 			getc(stdin);
			if (qtt>world.height*world.width)
				printf("\nMore cells than free space available. Please try again.");
			}while (qtt > world.height*world.width);

		//Game-world generation
		world=init(world);

		// Placing (random) the living cells
		while (total<qtt)
			{
			r=rand()%(world.height*world.width);
			i=r/world.width;
			j=r%world.width;
			if (world.matrix[i][j]==0)
				{
				world.matrix[i][j]=1;
				total++;
				// Placing a new cell... DONE
				}
			// else printf("\nA cell already exists here :: FAILED\n");
			}
		//Finished generating gameworld

		//Check is cell-age activated
		if (s_age==1)
			{
			do	{
				printf("Max age of a cell?\n");
				age=safe_read_pos_integer(age);
				} while (age<0);
			} 

		//Check if walls (non-viable cells) activated
		if ((s_walls==1)&&(qtt<world.width*world.height))
			{
			total=0;
			do	{
				printf("\nHow many non-viable cells do you want to introduce? (max possible %d)\n", world.width*world.height-qtt);
				dead=safe_read_pos_integer(dead);
				if (dead>world.width*world.height-qtt) 
					printf("\nMore non-viable cells than available cells");
				} while ((dead>world.width*world.height-qtt)||(dead<0));

				//This getc(stdin); is used to anihilate the enter after reading the number of walls to introduce
				//Needed for obstain the program from jumping directly to the 2nd stage
				getc(stdin);

			int r;	//used as generated random number
			
			//Placing the non-viable cells
 			while (total<dead)
				{
				r=rand()%(world.height*world.width);
				i=r/world.width;
				j=r%world.width;
				if (world.matrix[i][j]==0)
					{
					world.matrix[i][j]=-1;
					total++;
					}
				}
			}

			//Print the original gameworld (starting stage)
			system("clear");
			printf("\nStarting world: ");
			print(world, s_age);
			getc(stdin);
			//Start game with the created and modelated table
			etape_calc(world, qtt, s_torus, s_age, age);
		}

 	else //if game loaded
		//Procedure of reading the saved game-world
 		{
		printf("\nPlease introduce the path to the world to load:");
		scanf("%s",filename);
		printf("\nReading filename... DONE");
		system("clear");
		// Initializing myfile pointer
		FILE* myfile=NULL;
		//Opening the file
		myfile=fopen(filename,"r");
		if (myfile!=NULL)
			{
			// Reading world height and width
			fscanf(myfile,"\n%d %d\n",&world.height, &world.width);
			world=init(world);	// Initialize the game world
	
			// Reading the number of living cells to introduce
			fscanf(myfile, "%d \n",&qtt); // qtt = number of living cells
			for (k=0;k<qtt;k++)
				{
				fscanf(myfile, "%d %d \n",&i, &j);
				world.matrix[i][j]=1;
				}
	
			// Reading the number of non-viable cells to introduce
			fscanf(myfile, "%d \n",&dead); // dead = number of walls
			if (dead>0)
				{
				for (k=0;k<qtt;k++)
					{
					fscanf(myfile, "%d %d \n",&i, &j);
					world.matrix[i][j]=-1;
					}
				}
			// Closing the file
			fclose (myfile);
			
			do	{
				system("clear");
				TITLE
				printf("\n\tPlease select action:\n");
				printf("\t1. Start game\n");
		
				if (s_age==0) printf("\t2. Activate age of cells\n");
		 			else printf("\t2. Deactivate age of cells\n");
				if (s_torus==0) printf("\t3. Activate torus world\n");
		 			else printf("\t3. Deactivate torus world\n");
	
				scanf("%s",buffer);
				if (strlen(buffer)>1) 
					printf("\nInvalid choice, len=%d",strlen(buffer));
				else 	{
					sscanf(buffer, "%d", &choice);
				
					switch (choice)
						{
						case 1 :	break;
						case 2 :	{
 								if (s_age==0) 
									s_age=1; //Activate age of cells
 								else
			 						s_age=0; //Deactivate age of cells
								}	
								break;
						case 3 :	{
 								if (s_torus==0)
									s_torus=1; //Activate torus world
 								else
			 						s_torus=0; //Deactivate torus world
								}
								break;
						default :	{
								printf("\nInvalid choice");
								system("clear");
								}
						}
					}
				} while (choice!=1);

			//Check is cell-age activated
			if (s_age==1)
				{
				do	{
					printf("Max age of a cell?\n");
					age=safe_read_pos_integer(age);
					} while (age<0);
				} 

			//Print the original gameworld (starting stage)
			system("clear");
 			printf("Starting world: \n");
			print(world, s_age);

			getc(stdin);
			//Start game with the created and modelated table
			etape_calc(world, qtt, s_torus, s_age, age);
			}
		else //File not found
			{
			printf("\nFile doesn't exist\n");
			getc(stdin);
			printf("\nPress Enter to return to Main menu\n");
			getc(stdin);
			system("clear");
			menu();
			}
		}
	}

main ()
	{
	system("clear");
	menu();
	}
