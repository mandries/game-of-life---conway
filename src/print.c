#include <stdio.h>
#include <stdlib.h>
#include "calc.h"
#include "menu.h"
#include "print.h"

//Function used to print the grid (with the possibility to show the age of each cell)
void print (board a_in, int s_age)
	{
	int i,j;

	//printing the first line |-|-...-|-|	
	printf("\n");
	for (j=0;j<a_in.width;j++)
		printf("|---");
	printf("|\n");

	//printing the cells
	for (i=0;i<a_in.height;i++)
		{
		for (j=0;j<a_in.width;j++)
			if (s_age==1) //Age option activated
				{
				if (a_in.matrix[i][j]>0) //if cell alive
					{
					if (a_in.matrix[i][j]<10) 
						printf("| %d ",a_in.matrix[i][j]);
					else if (a_in.matrix[i][j]<100) 
						printf("|%d ",a_in.matrix[i][j]);
					else if (a_in.matrix[i][j]<1000) 
						printf("|%d",a_in.matrix[i][j]);
					else
						printf("| D ");
					}
				else if (a_in.matrix[i][j]==0) printf("|   "); //if cell dead
// 				else printf("| ? ");	//if cell non-viable
				else printf("| X ");	//if cell non-viable	
				}
			else
				{
				//Age option not activated
				if (a_in.matrix[i][j]>0) printf("| O "); //alive cell
				else 	if (a_in.matrix[i][j]==0) printf("|   "); //dead cell

					else printf("| X ");	//non-viable cell
				}
		
		printf("|\n");		//New line

		//printing the division line between lines (last bordering-line included)
		for (j=0;j<a_in.width;j++)
			printf("|---");
		printf("|\n");
		//end of division line
		}		
	}
	
