#include <stdio.h>
#include <stdlib.h>
#ifndef _CALC_H_
#define _CALC_H_

//Conway's game of life
typedef struct 
	{
	int height;
	int width;
	int **matrix;
	} board;

board init(board a_out);

void liberer(board a);

board etape_calc(board a_in, int qtt, int s_torus, int s_age, int age);

#endif
