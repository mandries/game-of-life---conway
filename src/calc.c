#include <stdio.h>
#include <stdlib.h>
#include "calc.h"
#include "print.h"
extern int stage;

//Fonction utilisee pour initialiser une table de jeux
board init (board a_out)
	{
	int i,j;
	a_out.matrix = (int**) (malloc (a_out.height*sizeof(int*)));
 	for (i=0;i<a_out.height;i++)
		a_out.matrix[i]=(int*) (malloc (a_out.width*sizeof(int)));

	for (i=0;i<a_out.height;i++)
		for (j=0;j<a_out.width;j++)
			a_out.matrix[i][j]=0;
	return a_out;
	}

//Function used to free memory after usage
void liberer(board a)
	{
	int i,j;
	for (i=0;i<a.height;i++)
		free(a.matrix[i]);
	free(a.matrix);
	}


//Calculating the next stage
board etape_calc (board a_in, int qtt, int s_torus, int s_age, int age)
	{
	board a_out;
	int i,j,kl,kc;
	int alive;	//Alive = number of cells alive around a cell a[i][j]
	int choice='a';
	int total_alive=0;	//Used to return to main menu if all the cells are dead
	a_out.width=a_in.width;
	a_out.height=a_in.height;
	a_out=init(a_out);
	
 	printf("\nSelect action:\n");
 	printf("\nEnter = Next Stage");
 	printf("\nq     = Exit to Main Menu\n");
	choice=getc(stdin);

	// q in ASCII = 113
	if (choice=='q') 
				{
				system("clear");
				menu();	//Exit to Main Menu
				}
	
	//Enter in ASCII = 13, "CR" or 015
	//	CR = 13	Chariot Return
	//	LF = 10	Line Feed
	//If Enter pressed, calculate and show next stage
	else if (choice==10)
		{
		//Torus world: Calculate next stage with/without age and dead cells
		if (s_torus==1)
			for (i=0;i<a_in.height;i++)
				for (j=0;j<a_in.width;j++)
					{
// 					printf("Calculating state for cell [%d][%d]",i,j);
					//Calculate the number of living cells around
					alive=0;
					//We search through the 9 cells, with our cell in center excluded
					for (kl=i-1;kl<=i+1;kl++)
						for (kc=j-1;kc<=j+1;kc++)
							{
							if (a_in.matrix[(kl+a_in.height)%a_in.height][(kc+a_in.width)%a_in.width]>0)
								{
								if ((kl!=i) || (kc!=j)) alive++;
								}
							}
					// If age option activated
					if (s_age==1)
						{
						if (a_in.matrix[i][j]==0) 
							{
							if (alive==3)
								{
								a_out.matrix[i][j]=1;
								total_alive++;
								}
							else 
								a_out.matrix[i][j]=0;
							}
						else 	if (a_in.matrix[i][j]>0) 
							{
							if ((alive==2) || (alive==3))
									{
									if (a_in.matrix[i][j]<age)
										{
										a_out.matrix[i][j]=a_in.matrix[i][j]+1;
										total_alive++;
										}
									else 
										a_out.matrix[i][j]=0;
									}
							else
								a_out.matrix[i][j]=0;
							}
						else a_out.matrix[i][j]=-1;	//Wall (non-viable cell)
						}
					
					else //(s_age==0) age option not activated
						{
						if (a_in.matrix[i][j]==0)
							{
							if (alive==3)
								{
								a_out.matrix[i][j]=1;
								total_alive++;
								}
							else
								a_out.matrix[i][j]=0;
							}
						else 	if (a_in.matrix[i][j]>0)
								{
								if ((alive==2) || (alive==3))
									{
									a_out.matrix[i][j]=a_in.matrix[i][j];
									total_alive++;
									}
								else
									a_out.matrix[i][j]=0;
								}
						else a_out.matrix[i][j]=-1;	//Wall
						}
					}
					
		//Not torus-like world: Calculate next stage with/without age and dead cells
		else	
		//Plane world algorithm
			for (i=0;i<a_in.height;i++)
				for (j=0;j<a_in.width;j++)
					{
					//Calculate the number of living cells around
					alive=0;
					//We search through the 9 cells, with our cell in center excluded
					//Borders create a problem: how to exclude the out-bordered cells
					if (i==0)	//Top row
						{
						for (kl=i;(kl<=i+1)&&(kl<a_in.height);kl++)
							if (j==0)	//Top-left corner
								{
								for (kc=j;kc<=j+1;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
										alive++;
								}
 							else if (j==a_in.width-1)	//Top-right corner
								{
								for (kc=j-1;kc<=j;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
								 	alive++;
								}
							else	//Top row, middle cells
								{
								for (kc=j-1;kc<=j+1;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
								 	alive++;
								}	
						}	

					else if (i==a_in.height-1)	//Bottom row
						{
						for (kl=i-1;kl<=i;kl++)
							if (j==0)	//Bottom-left corner
								{
								for (kc=j;(kc<=j+1)&&(kc<a_in.width);kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
									alive++;
								}
							else if (j==a_in.width-1)	//Bottom-right corner
								{
								for (kc=j-1;kc<=j;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
								 	alive++;
								}
							else		//Bottom middle cells
								{
								for (kc=j-1;kc<=j+1;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
								 	alive++;
								}	
						}

					else //Middle lines
						{
						for (kl=i-1;kl<=i+1;kl++)
							if (j==0)	//Left border
								{
								for (kc=j;kc<=j+1;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
									alive++;
								}
							else if (j==a_in.width-1)	//Right border
								{
								for (kc=j-1;kc<=j;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
								 	alive++;
								}
							else		//Middle cells
								{
								for (kc=j-1;kc<=j+1;kc++)
								if ((a_in.matrix[kl][kc]>0)&&((kl!=i) || (kc!=j)))
								 	alive++;
								}	
						}
				if (s_age==1)	//Age option activated
					{
					//If the cell is dead
					if (a_in.matrix[i][j]==0) 
						{
						if (alive==3) // having 3 living cells around
							{
							a_out.matrix[i][j]=1;	//Give life
							total_alive++;
							}
						else 
							a_out.matrix[i][j]=0;	//Leave dead
						}
					//Living cell
					else 	if (a_in.matrix[i][j]>0)
							{
							// Neighbours: 2 or 3
							if ((alive==2) || (alive==3))
								{
								if (a_in.matrix[i][j]<age)
									{
									a_out.matrix[i][j]=a_in.matrix[i][j]+1;
									total_alive++;
									}
								else 
									a_out.matrix[i][j]=0;
								}
							// More or less neighbours
							else
								a_out.matrix[i][j]=0; //Kill cell
							}
					//Wall	
					else a_out.matrix[i][j]=-1;
					}
				else //(s_age==0) age not activated
					{
					if (a_in.matrix[i][j]==0)
						{ 
						if (alive==3)
							{
							a_out.matrix[i][j]=1;
							total_alive++;
							}
						else
							a_out.matrix[i][j]=0;
						}
					else 	if (a_in.matrix[i][j]>0) 
							{
							if ((alive==2) || (alive==3))
								{
								a_out.matrix[i][j]=a_in.matrix[i][j];
								total_alive++;
								}
							else
								a_out.matrix[i][j]=0;
							}
						else a_out.matrix[i][j]=-1;	//Wall
					}
				}
		//Calculating next stage for this world... DONE
		liberer(a_in);
		}
	else
		{
		printf("\n Invalid choice");
		etape_calc (a_in, qtt, s_torus, s_age, age);	//Recursive part
		}

	//Afficher le tableau
	stage++;
	// printing the Stage number
	system("clear");
	printf("Stage %d\n",stage);
	print (a_out, s_age);

	//If no more living cells - return to main menu
	if (total_alive==0)
		{
		printf("\nNo more living cells (apocalipsis reached), returning to Main menu...\n");
		//Another enter
		getc(stdin);
		system("clear");
		menu();
		}
	else
	etape_calc (a_out, qtt, s_torus, s_age, age);	//Recursice part
	}
